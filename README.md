# Pandoc Template for Educational Statements

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/statement/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/statement/commits/master)

## Description

This project provides a template for writing educational statements in
Markdown and exporting them to PDF files using [Pandoc](https://pandoc.org).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To write a statement, [Pandoc](https://pandoc.org) at least 2.0 must be
installed on your computer.

## Creating your Statement

To create your own statement, you need to setup its YAML configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.

Read below for details on how to configure your statement.
You can also see our [example](example), with its
[resulting PDF](/../builds/artifacts/master/file/example.pdf?job=make-example).

### LaTeX Preamble

The parameter `language` allows to specify the language of the document, which
is set to english by default.

You may also load additional packages by specifying them in the list
`packages`.

### Informations about the University

The parameters `institute` and `logo` specify the university in which the
course is taught and its logo, respectively.

### Informations about the Degree

The parameters `degree`, `semester` and `academic-year` allow respectively to
specify the name of the degree, the semester and the academic year in which
the course is taught.

### Informations about the Course

The parameters `course` and `acronym` specify the name of the course and
its acronym.

The parameters `type` and `number` specify the type of the document and the
number identifying this document.

Finally, the parameter `title` specify the title of the document (which is
displayed in the header).
This parameter is optional.

## Building the Statement

Suppose that you have written your statement in a file `input.md`.
Then, to export this statement to a PDF file named `output.pdf`, execute the
following command:

```bash
pandoc --template statement.pandoc input.md --output output.pdf
```
