---
# Informations about the University.
institute: University of Pandoc
logo: images/markdown.png

# Informations about the Degree.
degree: LaTeX Certification
semester: First Semester
academic-year: 2019 -- 2020

# Informations about the Course.
course: Pandoc Toolkit
acronym: PTK
type: Example
number: 0
title: Release 0.1.0
---

# Example of Statement

*This document illustrates how statements are rendered with Pandoc.*

## Some Text

Here is a short paragraph to fill this document.
And, since we need more text, let us put some lorem ipsum dolor sit amet,
consectetur adipiscing elit.
Nam sit amet urna id nibh commodo feugiat.
Aenean malesuada, eros quis tincidunt rhoncus, metus ex feugiat leo, ut
sollicitudin arcu est quis ante.
Donec vehicula ac neque eget suscipit.
Proin accumsan condimentum pulvinar.
Suspendisse vehicula nibh magna, at congue dui porttitor sit amet.
Vivamus sit amet lorem dignissim, fringilla ligula sit amet, molestie nunc.

Since this statement is written in Markdown, you can use all its typography
variants, namely:

+ *italic* text, or
+ **bold** text.

And, since we need even more text, let us put some lorem ipsum dolor sit amet,
consectetur adipiscing elit.
Nam sit amet urna id nibh commodo feugiat.
Aenean malesuada, eros quis tincidunt rhoncus, metus ex feugiat leo, ut
sollicitudin arcu est quis ante.
Donec vehicula ac neque eget suscipit.
Proin accumsan condimentum pulvinar.
Suspendisse vehicula nibh magna, at congue dui porttitor sit amet.
Vivamus sit amet lorem dignissim, fringilla ligula sit amet, molestie nunc.

## Figures

Markdown allows to integrate different kinds of figures.
As an example, it is really easy to integrate images in your documents.

![Pandoc uses LaTeX to produce this statement.](images/latex.png)

You may also add tables, either using the native Markdown support for them,
or the [variant recognized by Pandoc](https://pandoc.org/MANUAL.html#tables).

| City         | Population  |
|--------------|-------------|
| Mexico City  | 20,116,842  |
| Shanghai     | 19,210,000  |
| Peking       | 15,796,450  |
| Istanbul     | 14,160,467  |

## Source Code

Finally, you can write source code in your statements.
As an example, here is an *Hello, World!* written in Java.

```java
/**
 * A short class showing how to write a piece of code in a statement.
 */
public class Example {

    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

}
```

## Conclusion

Let us make a short recap of what this template provides.

1. You can easily configure your statements following the needs of your course.
2. You can write your statements in plain Markdown.
3. You can add different kinds of figures, such as images and tables.
4. You can write code and benefit from Pandoc's syntax highlighting.
