---
# LaTeX Configuration.
language:
packages:
    -

# Informations about the University.
institute:
logo:

# Informations about the Degree.
degree:
semester:
academic-year:

# Informations about the Course.
course:
acronym:
type:
number:
title:
---
